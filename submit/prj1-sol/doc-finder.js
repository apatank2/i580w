const {inspect} = require('util'); //for debugging

'use strict';

class DocFinder {

  /** Constructor for instance of DocFinder. */
  constructor() {
    //@TODO
    this.NoiseWords = new Map();
    this._Document = new Map();
    this._DocContent = new Map();
    this._CompleteWord = new Map();
    this._ResultArray = new Array();
    this._OffsetWord = new Map();
  }

  SortAs(a,b) { return a-b; }
 
  /** Return array of non-noise normalized words from string content.
   *  Non-noise means it is not a word in the noiseWords which have
   *  been added to this object.  Normalized means that words are
   *  lower-cased, have been stemmed and all non-alphabetic characters
   *  matching regex [^a-z] have been removed.
   */
  words(content) {
    //@TODO
    var wordsArray = content.split(/\s+/);
    var nonnoisyeword = [];
    for (var i in wordsArray) {
    var _noisyWord1 = normalize(wordsArray[i]);
    if (this.NoiseWords.has(_noisyWord1))
    {}
    else
    {
     nonnoisyeword.push(_noisyWord1);
     }
    }
    return nonnoisyeword;
  }

  /** Add all normalized words in noiseWords string to this as
   *  noise words. 
   */
  addNoiseWords(noiseWords) {
    //@TODO
    var a = new Array();
    a = noiseWords.split("\n"); 
    var i;
    for (i = 0; i < a.length; i++) 
    { 
     var _normword = normalize(a[i]);
     if (_normword !== null)
     {
     this.NoiseWords.set(_normword);
     }
    }
	      			    
  }

  /** Add document named by string name with specified content to this
   *  instance. Update index in this with all non-noise normalized
   *  words in content string.
   */ 
  addContent(name, content) {
    //@TODO
     this._Document.set(name,content);
     var _wordsSplit = content.split("\n");
     for (var j = 0; j < _wordsSplit.length; j++) 
     {
     var _wordsArray = _wordsSplit[j].split(/\s+/);
     for (var i = 0; i < _wordsArray.length; i++) 
     {
     var _normword = normalize(_wordsArray[i]);
     if (this._DocContent.has(name.concat(";",_normword)))
     {
     var _count = this._DocContent.get(name.concat(";",_normword)) + 1;
     this._DocContent.set(name.concat(";",_normword),_count);
     }
     else
     {
     this._DocContent.set(name.concat(";",_normword),1);
     this._CompleteWord.set(_normword,_normword);
     this._OffsetWord.set(name.concat(";",_normword),j);
     }
     }
     }
  }

  /** Given a list of normalized, non-noise words search terms, 
   *  return a list of Result's  which specify the matching documents.  
   *  Each Result object contains the following properties:
   *     name:  the name of the document.
   *     score: the total number of occurrences of the search terms in the
   *            document.
   *     lines: A string consisting the lines containing the earliest
   *            occurrence of the search terms within the document.  Note
   *            that if a line contains multiple search terms, then it will
   *            occur only once in lines.
   *  The Result's list must be sorted in non-ascending order by score.
   *  Results which have the same score are sorted by the document name
   *  in lexicographical ascending order.
   *
   */
  find(terms) {
    //@TODO
     this._ResultArray = [];
     if (terms.length > 0 )
     {
     var _WordCnt = 0;
     var content; 
     for (var [key] of this._Document)
     {  
      _WordCnt = 0;
      var _AddedLine;
      content = this._Document.get(key);
      var _wordsArray = content.split("\n");
      var _SentenceStore = new Array();
      for (var k = 0; k < terms.length; k++) 
      {
       if (this._DocContent.has(key.concat(";",terms[k])))
       {
       if(this._DocContent.get(key.concat(";",terms[k])) > 0 )
       { 
        _SentenceStore.push(Number(this._OffsetWord.get(key.concat(";",terms[k]))));
       }
       _WordCnt = _WordCnt + this._DocContent.get(key.concat(";",terms[k]));
       }
            
       }
      if (_WordCnt > 0)
      {
        _SentenceStore = _SentenceStore.sort(this.SortAs);
        for (var z = 0; z < _SentenceStore.length; z++) {
        if (_AddedLine == null)
          {
           _AddedLine = _wordsArray[_SentenceStore[z]] + "\n";
          }
          else
          {
            if (_AddedLine.includes(_wordsArray[_SentenceStore[z]]) === true)
            {}
            else
            {
            _AddedLine = _AddedLine + _wordsArray[_SentenceStore[z]] + "\n";
            }
          }
          }
      if (_AddedLine != null){
      const _Result = new Result(key,_WordCnt,_AddedLine);
      this._ResultArray.push(_Result);
      _AddedLine = null;
      }
      else
      {
      const _Result = new Result(key,_WordCnt);
      this._ResultArray.push(_Result);
      }
      }
      }
    this._ResultArray = this._ResultArray.sort(compareResults);
    }
    return this._ResultArray;
  }
  

  /** Given a text string, return a ordered list of all completions of
   *  the last word in text.  Returns [] if the last char in text is
   *  not alphabetic.
   */
  complete(text) {
    //@TODO
    var _wordarray = new Array();
    if (text !== "")
    {
    var _text = text.toLowerCase().split(/\s+/);
    var _textlength = _text.length;
    if (_textlength > 0 )
    { 
      for (var [key] of this._CompleteWord)
     {  
       if(key.startsWith(_text[_textlength - 1]) === true)
       {
         _wordarray.push(key);
       }
     }     
    }
    }
    return _wordarray;
  }

  
} //class DocFinder

module.exports = DocFinder;

/** Regex used for extracting words as maximal non-space sequences. */
const WORD_REGEX = /\S+/g;

/** A simple class which packages together the result for a 
 *  document search as documented above in DocFinder.find().
 */ 
class Result {
  constructor(name, score, lines) {
    this.name = name; this.score = score; this.lines = lines;
  }

  toString() { return `${this.name}: ${this.score}\n${this.lines}`; }
}

/** Compare result1 with result2: higher scores compare lower; if
 *  scores are equal, then lexicographically earlier names compare
 *  lower.
 */
function compareResults(result1, result2) {
  return (result2.score - result1.score) ||
    result1.name.localeCompare(result2.name);
}

/** Normalize word by stem'ing it, removing all non-alphabetic
 *  characters and converting to lowercase.
 */
function normalize(word) {
  return stem(word.toLowerCase()).replace(/[^a-z]/g, '');
}

/** Place-holder for stemming a word before normalization; this
 *  implementation merely removes 's suffixes.
 */
function stem(word) {
  return word.replace(/\'s$/, '');
}



