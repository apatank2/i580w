'use strict';

const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const process = require('process');
const url = require('url');
const queryString = require('querystring');

const OK = 200;
const CREATED = 201;
const BAD_REQUEST = 400;
const NOT_FOUND = 404;
const CONFLICT = 409;
const SERVER_ERROR = 500;


//Main URLs
const DOCS = '/docs';
const COMPLETIONS = '/completions';

//Default value for count parameter
const COUNT = 5;

/** Listen on port for incoming requests.  Use docFinder instance
 *  of DocFinder to access document collection methods.
 */
function serve(port, docFinder) {
  const app = express();
  app.locals.port = port;
  app.locals.finder = docFinder;
  setupRoutes(app);
  const server = app.listen(port, async function() {
    console.log(`PID ${process.pid} listening on port ${port}`);
  });
  return server;
}

module.exports = { serve };

function setupRoutes(app) {
  const port = app.locals.port;
  //console.log(port);
  app.use(cors());            //for security workaround in future projects
  app.use(bodyParser.json()); //all incoming bodies are JSON
  //@TODO: add routes for required 4 services
  //const base = app.locals.base;
  app.post(`${DOCS}`, doAddDoc(app));
  app.get(`${DOCS}/:name`, getDocContent(app));
  app.get(`${COMPLETIONS}`, getCompletion(app));
  app.get(`${DOCS}`, getFind(app));
  app.use(doErrors()); //must be last; setup for server errors   
}

//@TODO: add handler creation functions called by route setup
//routine for each individual web service.  Note that each
//returned handler should be wrapped using errorWrap() to
//ensure that any internal errors are handled reasonably.
function getDocContent(app) {
  return errorWrap(async function(req, res) {
    try {
      const {name} = req.params;
      const result = await app.locals.finder.docContent(name);
      var results = {"content" : result,"links": [{"rel": "self","href": baseUrl(req,DOCS+'/'+name)}]};
      res.json(results);
    }
    catch (err) {
      if (err.code === 'NOT_FOUND' )
      {
       const error = { code: 'NOT_FOUND', message: `doc ${req.params.name} not found ` } 
       res.status(NOT_FOUND).json(error);
      }
      else
      {
      const mapped = mapError(err);
      res.status(mapped.status).json(mapped);}
    }
  });
}

function getCompletion(app) {
  return errorWrap(async function(req, res) {
    try {
      if (typeof req.query['text'] === 'undefined')
      {
       const error1 = { code: 'BAD_PARAM', message: `required query parameter \"text\" is missing` } ;
       res.status(BAD_REQUEST).json(error1);
       return;
      }
      const name = req.query.text;
      const result = await app.locals.finder.complete(name);
      //var results = {"content" : result,"links": [{"rel": "self","href": baseUrl(req,DOCS)}]};
      res.json(result);
    }
    catch (err) {
      const mapped = mapError(err);
      res.status(mapped.status).json(mapped);
    }
  });
}

function getFind(app) {
  return errorWrap(async function(req, res) {
    try {
      //console.log(req)
      const defaultCount = COUNT;
      //console.log(req.query['q']);
      const name = req.query.q;
      if (typeof req.query['q'] === 'undefined')
      {
       const error1 = { code: 'BAD_PARAM', message: `required query parameter \"q\" is missing` } ;
       res.status(BAD_REQUEST).json(error1);
       return;
      }
      if (typeof req.query['start'] === 'undefined')
      {
      }
      else if(isNaN(req.query.start) || parseInt(req.query.start) < 0 )
      {
       const error1 = { code: 'BAD_PARAM', message: `bad query parameter \"start\"` } ;
       res.status(BAD_REQUEST).json(error1);
       return;
      }
      if (typeof req.query['count'] === 'undefined')
      {
      }
      else if(isNaN(req.query.count) || parseInt(req.query.count) < 0 )
      {
       const error1 = { code: 'BAD_PARAM', message: `bad query parameter \"count\"` } ;
       res.status(BAD_REQUEST).json(error1);
       return;
      }
      var count = parseInt(req.query.count);
      var start = parseInt(req.query.start);
      // console.log(isNaN(req.query.start));
      if (isNaN(count))
      count = defaultCount;
      if (isNaN(start))
      start = 0;
      var result = await app.locals.finder.find(name);
      var resultdata = [];
      var arrayCount = 0;
      for (var i = start; i < Math.min((start + count),result.length) ; i++)
      {
       resultdata[arrayCount] = { name : result[i].name, score : result[i].score, lines : result[i].lines, href : baseUrl(req,DOCS+'/'+result[i].name) };
       arrayCount++;
      }
      var finalresult = {results : resultdata, totalCount : result.length, links : []};
      var next_prev_url = req.url.split('&');
      var linkcount = 0;
      if (next_prev_url[0].includes("q="))
      finalresult.links[linkcount] = { rel: "self" ,href: baseUrl(req,next_prev_url[0]+'&start='+start+'&count='+count) };
      else
      finalresult.links[linkcount] = { rel: "self" ,href: baseUrl(req,next_prev_url[0]+'=&start='+start+'&count='+count) };
      linkcount++;
      if (start === 0 && (start + count) < result.length)
      {
      var n = start + count;
      if (result.length > count)
      finalresult.links[linkcount] = { rel: "next" ,href: baseUrl(req,next_prev_url[0]+'&start='+n+'&count='+count) };
      }
      else if (start > 0 )
      {
      var n = start + count;
      if (result.length > n)
      {
      finalresult.links[linkcount] = { rel: "next" ,href: baseUrl(req,next_prev_url[0]+'&start='+n+'&count='+count) }; 
      linkcount++;
      }
      var n = start - count;
      finalresult.links[linkcount] = { rel: "previous" ,href: baseUrl(req,next_prev_url[0]+'&start='+(n > 0 ? n : 0)+'&count='+ count)};
      }
      res.json(finalresult);
    }
    catch (err) {
      const mapped = mapError(err);
      res.status(mapped.status).json(mapped);
    }
  });
}


function doAddDoc(app) {
  return errorWrap(async function(req, res) {
    try {
      const obj = req.body;
      if (typeof obj.name === 'undefined')
      {
       const error1 = { code: 'BAD_PARAM', message: `required body parameter \"name\" is missing` } ;
       res.status(BAD_REQUEST).json(error1);
       return;
      }
      if (typeof obj.content === 'undefined')
      {
       const error1 = { code: 'BAD_PARAM', message: `required body parameter \"content\" is missing` } ;
       res.status(BAD_REQUEST).json(error1);
       return;
      }
      const results = await app.locals.finder.addContent(obj.name,obj.content);
      const port = req.app.locals.port;
      var res1 = {href : baseUrl(req,DOCS+'/'+obj.name) };
      //console.log(baseUrl(req,DOCS+'/'+obj.name) );
      res.append('Location', baseUrl(req,DOCS+'/'+obj.name) );
      res.status(CREATED).json(res1);
    }
    catch(err) {
      const mapped = mapError(err);
      res.status(mapped.status).json(mapped);
    }
  });
}
/** Return error handler which ensures a server error results in nice
 *  JSON sent back to client with details logged on console.
 */ 
function doErrors(app) {
  return async function(err, req, res, next) {
    res.status(SERVER_ERROR);
    res.json({ code: 'SERVER_ERROR', message: err.message });
    console.error(err);
  };
}

/** Set up error handling for handler by wrapping it in a 
 *  try-catch with chaining to error handler on error.
 */
function errorWrap(handler) {
  return async (req, res, next) => {
    try {
      await handler(req, res, next);
    }
    catch (err) {
      next(err);
    }
  };
}
  
const ERROR_MAP = {
  EXISTS: CONFLICT,
  NOT_FOUND: NOT_FOUND,
}

/** Map domain/internal errors into suitable HTTP errors.  Return'd
 *  object will have a "status" property corresponding to HTTP status
 *  code.
 */
function mapError(err) {
  console.error(err);
  return err.isDomain
    ? { status: (ERROR_MAP[err.errorCode] || BAD_REQUEST),
	code: err.errorCode,
	message: err.message
      }
    : { status: SERVER_ERROR,
	code: 'INTERNAL',
	message: err.toString()
      };
} 
/** Return base URL of req for path.
 *  Useful for building links; Example call: baseUrl(req, DOCS)
 */
function baseUrl(req, path='/') {
  const port = req.app.locals.port;
  const url = `${req.protocol}://${req.hostname}:${port}${path}`;
  return url;
}
