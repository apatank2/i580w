const assert = require('assert');
const mongo = require('mongodb').MongoClient;

const {inspect} = require('util'); //for debugging

'use strict';

/** This class is expected to persist its state.  Hence when the
 *  class is created with a specific database url, it is expected
 *  to retain the state it had when it was last used with that URL.
 */ 
class DocFinder {

  /** Constructor for instance of DocFinder. The dbUrl is
   *  expected to be of the form mongodb://SERVER:PORT/DB
   *  where SERVER/PORT specifies the server and port on
   *  which the mongo database server is running and DB is
   *  name of the database within that database server which
   *  hosts the persistent content provided by this class.
   */
  constructor(dbUrl) {
    //TODO
   this.client;
   this.db;
   let regex1 = new RegExp("^(.*[\\\/])");
   regex1 = dbUrl.split(regex1);
   this.MONGOURL = regex1[1];
   this.DBNAME = regex1[2];
   this.DOC_TABLE = 'documents';
   this.NOISYWORD_TABLE = 'noisywords';
   this.DOCUMENT_INFO = 'docinfo';
  }
  
  
  /** This routine is used for all asynchronous initialization
   *  for instance of DocFinder.  It must be called by a client
   *  immediately after creating a new instance of this.
   */
  async init() {
    //TODO	
   this.client = await mongo.connect(this.MONGOURL,MONGO_OPTIONS);
   this.db = this.client.db(this.DBNAME);
  }

  /** Release all resources held by this doc-finder.  Specifically,
   *  close any database connections.
   */
  async close() {
    //TODO
    this.client.close();
  }

  /** Clear database */
  async clear() {
    //TODO
    if (await this.db.collection(this.DOC_TABLE).find({}).count() > 0 )
    await this.db.collection(this.DOC_TABLE).drop();
    if (await this.db.collection(this.NOISYWORD_TABLE).find({}).count() > 0 )
    await this.db.collection(this.NOISYWORD_TABLE).drop();
    if (await this.db.collection(this.DOCUMENT_INFO).find({}).count() > 0 )
    await this.db.collection(this.DOCUMENT_INFO).drop();
  }

  /** Return an array of non-noise normalized words from string
   *  contentText.  Non-noise means it is not a word in the noiseWords
   *  which have been added to this object.  Normalized means that
   *  words are lower-cased, have been stemmed and all non-alphabetic
   *  characters matching regex [^a-z] have been removed.
   */
  async words(contentText) {
    //TODO
    var wordsArray = contentText.split(/\s+/);
    var nonnoisyeword = [];
    for (var i in wordsArray) {
    var _noisyWord1 = normalize(wordsArray[i]);
    if ((await this.db.collection(this.NOISYWORD_TABLE).find({value : _noisyWord1}).count()) === 0)
    {
     nonnoisyeword.push(_noisyWord1);
     }
    }
    return nonnoisyeword;
  }

  /** Add all normalized words in the noiseText string to this as
   *  noise words.  This operation should be idempotent.
   */
  async addNoiseWords(noiseText) {
    //TODO
    var noiseWords = new Set();
    await this.db.collection(this.NOISYWORD_TABLE).find().forEach( function(NoisyWrd) { noiseWords.add( NoisyWrd.value); } );
    await this.db.collection(this.NOISYWORD_TABLE).deleteMany({})
    var a = [];
    a = noiseText.split("\n"); 
    var i;
    for (i = 0; i < a.length; i++) 
    { 
     var _normword = normalize(a[i]);
     if (_normword !== null)
     {
     noiseWords.add(_normword);
     }
    }
    var _words = [];
    noiseWords.forEach( function (value) 
    { 
     _words.push(value);
    } );
    var listOfObjects = [];
    _words.forEach(function(entry) {
    var singleObj = {}
    singleObj['type'] = 'noisewords';
    singleObj['value'] = entry;
    listOfObjects.push(singleObj);
    });
    await this.db.collection(this.NOISYWORD_TABLE).insertMany(listOfObjects, function(err, res) {
    });
   }

  /** Add document named by string name with specified content string
   *  contentText to this instance. Update index in this with all
   *  non-noise normalized words in contentText string.
   *  This operation should be idempotent.
   */ 
  async addContent(name, contentText) {
    //TODO
    if ((await this.db.collection(this.DOC_TABLE).find({documentname : name}).count()) === 0)
    {
    const documenttable = this.db.collection(this.DOC_TABLE);
    await documenttable.insertOne( { documentname : name, description : contentText } );
    }
    else
    {
    await this.db.collection(this.DOC_TABLE).updateOne({'documentname':name},
    {$set:{'description':contentText}},{multi:true})
    }
    if ((await this.db.collection(this.DOCUMENT_INFO).find({DocumentName : name}).count()) > 0)
    {
    await this.db.collection(this.DOCUMENT_INFO).deleteMany({DocumentName : name});
    }
     var _DocContent = new Map();
     var _OffsetWord = new Map();
     var _words = [];
     var _wordsSplit = contentText.split("\n");
     for (var j = 0; j < _wordsSplit.length; j++) 
     {
     var _wordsArray = _wordsSplit[j].split(/\s+/);
     for (var i = 0; i < _wordsArray.length; i++) 
     {
     var _normword = normalize(_wordsArray[i]);
     if (_DocContent.has(name.concat(";",_normword)))
     {
     var _count = _DocContent.get(name.concat(";",_normword)) + 1;
      _DocContent.set(name.concat(";",_normword),_count);
     }
     else
     {
     _DocContent.set(name.concat(";",_normword),1);
     _OffsetWord.set(name.concat(";",_normword),j);
     _words.push(name.concat(";",_normword));
     }
     }
     }
     var listOfObjects = [];
     _words.forEach(function(entry) {
     var singleObj = {}
     singleObj['WordCount'] = _DocContent.get(entry);
     singleObj['Offset'] = _OffsetWord.get(entry);
     var _name = entry.split(";");
     singleObj['DocumentName'] = _name[0];
     singleObj['Word'] = _name[1];
     listOfObjects.push(singleObj);
     });
     await this.db.collection(this.DOCUMENT_INFO).insertMany(listOfObjects, function(err, res) {
     });
  }

  /** Return contents of document name.  If not found, throw an Error
   *  object with property code set to 'NOT_FOUND' and property
   *  message set to `doc ${name} not found`.
   */
  async docContent(name) {
    //TODO
    if ((await this.db.collection(this.DOC_TABLE).find({documentname : name}).count()) === 0 )
    {
    throw  `doc ${name} not found`;
    }
    else
    {
    let description = await this.db.collection(this.DOC_TABLE).findOne({documentname : name},{description: true,_id:false});
    return  description['description'];
    }
  }
  
  /** Given a list of normalized, non-noise words search terms, 
   *  return a list of Result's  which specify the matching documents.  
   *  Each Result object contains the following properties:
   *
   *     name:  the name of the document.
   *     score: the total number of occurrences of the search terms in the
   *            document.
   *     lines: A string consisting the lines containing the earliest
   *            occurrence of the search terms within the document.  The 
   *            lines must have the same relative order as in the source
   *            document.  Note that if a line contains multiple search 
   *            terms, then it will occur only once in lines.
   *
   *  The returned Result list must be sorted in non-ascending order
   *  by score.  Results which have the same score are sorted by the
   *  document name in lexicographical ascending order.
   *
   */
  async find(terms) {
    //TODO
     var _Document = new Map();
     var _DocContent = new Map();
     var _Sentence = new Map();
     var _OffsetWord = new Map();
     var _ResultArray = [];
     for (var k = 0; k < terms.length; k++) 
     {
     await this.db.collection(this.DOCUMENT_INFO).find({Word : terms[k]}).forEach( function(Doc) 
     { 
     _Document.set( Doc.DocumentName); 
     _DocContent.set(Doc.DocumentName+";"+terms[k],Doc.WordCount);
     _OffsetWord.set(Doc.DocumentName+";"+terms[k],Doc.Offset);
     } );
     }
     var _WordCnt = 0;
     for (var [key] of _Document)
     {  
      _WordCnt = 0;
      var _AddedLine;
      var _SentenceStore = new Array();
      let description = await this.db.collection(this.DOC_TABLE).findOne({documentname : key},{description: true,_id:false});
      description = description['description'];
      var _wordsArray = description.split("\n");;
      for (var k = 0; k < terms.length; k++) 
      {
       if (_DocContent.has(key.concat(";",terms[k])))
       {
       if(_DocContent.get(key.concat(";",terms[k])) > 0 )
       { 
        _SentenceStore.push(Number(_OffsetWord.get(key.concat(";",terms[k]))));
       }
       _WordCnt = _WordCnt + _DocContent.get(key.concat(";",terms[k]));
       }
            
       }
      if (_WordCnt > 0)
      {
        _SentenceStore = _SentenceStore.sort(SortAs);
        for (var z = 0; z < _SentenceStore.length; z++) {
        if (_AddedLine == null)
          {
           _AddedLine = _wordsArray[_SentenceStore[z]] + "\n";
          }
          else
          {
            if (_AddedLine.includes(_wordsArray[_SentenceStore[z]]) === true)
            {}
            else
            {
            _AddedLine = _AddedLine + _wordsArray[_SentenceStore[z]] + "\n";
            }
          }
          }
      if (_AddedLine != null){
      const _Result = new Result(key,_WordCnt,_AddedLine);
      _ResultArray.push(_Result);
      _AddedLine = null;
      }
      else
      {
      const _Result = new Result(key,_WordCnt);
      _ResultArray.push(_Result);
      }
      }
      }
    _ResultArray = _ResultArray.sort(compareResults);
    return _ResultArray;
  }

  /** Given a text string, return a ordered list of all completions of
   *  the last normalized word in text.  Returns [] if the last char
   *  in text is not alphabetic.
   */
  async complete(text) {
    //TODO
    var _text = text.toLowerCase().split(/\s+/);
    var _textlength = _text.length;
    let _wordarray = new Set();
    if (_textlength > 0 )
    { 
      let search = _text[_textlength - 1];
      let regex1 = new RegExp("^"+search);
      await this.db.collection(this.DOCUMENT_INFO).find({Word : {$regex :regex1,$options : "i"}}).forEach( function(Doc) 
     { 
     _wordarray.add(Doc.Word);
     } ); 
       
    }
   return Array.from(_wordarray);
  }

  //Add private methods as necessary

} //class DocFinder

module.exports = DocFinder;

//Add module global functions, constants classes as necessary
//(inaccessible to the rest of the program).

//Used to prevent warning messages from mongodb.
const MONGO_OPTIONS = {
  useNewUrlParser: true
};

/** Regex used for extracting words as maximal non-space sequences. */
const WORD_REGEX = /\S+/g;

/** For Sorting. */
function SortAs(a,b) { return a-b; }

/** A simple utility class which packages together the result for a
 *  document search as documented above in DocFinder.find().
 */ 
class Result {
  constructor(name, score, lines) {
    this.name = name; this.score = score; this.lines = lines;
  }

  toString() { return `${this.name}: ${this.score}\n${this.lines}`; }
}

/** Compare result1 with result2: higher scores compare lower; if
 *  scores are equal, then lexicographically earlier names compare
 *  lower.
 */
function compareResults(result1, result2) {
  return (result2.score - result1.score) ||
    result1.name.localeCompare(result2.name);
}

/** Normalize word by stem'ing it, removing all non-alphabetic
 *  characters and converting to lowercase.
 */
function normalize(word) {
  return stem(word.toLowerCase()).replace(/[^a-z]/g, '');
}

/** Place-holder for stemming a word before normalization; this
 *  implementation merely removes 's suffixes.
 */
function stem(word) {
  return word.replace(/\'s$/, '');
}




