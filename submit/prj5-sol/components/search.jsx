//-*- mode: rjsx-mode;

'use strict';

const React = require('react');

class Search extends React.Component {

  /** called with properties:
   *  app: An instance of the overall app.  Note that props.app.ws
   *       will return an instance of the web services wrapper and
   *       props.app.setContentName(name) will set name of document
   *       in content tab to name and switch to content tab.
   */
  constructor(props) {
    super(props);
    //@TODO
    this.state={ value:'',output : [] ,totalCount:0,error:''} ;
    this.outputs;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.toggleState = this.toggleState.bind(this);
  //  this.createTable = this.createTable.bind(this);

  }

  //@TODO
   

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  async handleSubmit(event) {
    //alert('A name was submitted: ' + this.state.value);
    try {
    event.preventDefault();
    
    if(this.state.value.replace(/\s/g, '').length){
    this.setState({output : [] ,totalCount:0});
    this.outputs = await this.props.app.ws.searchDocs(this.state.value,0);
     // console.log(this.outputs.totalCount);
     if(this.outputs.totalCount > 0){
     //console.log("Inside if");
     this.setState({error: ''});
     this.setState({output: this.outputs.results});
     this.setState({totalCount: this.outputs.totalCount});
     }
     else
     {
     //console.log("Not Inside if");
      var err = `No results for ${this.state.value}`;
      this.setState({output : '' ,totalCount:0});
      this.setState({error : err});
     }
     }
     else
     {
     this.setState({output : '' ,totalCount:0,error:''});
     }
     }
     catch (err) {
       const msg = (err.message) ? err.message : 'web service error';
       this.setState({errors: [msg]});
     }
  }
  
  returnLines(n)
  {
  let lines = [];
  var words = this.state.value.split(" ");

  for (let i = 0; i < this.state.output[n].lines.length; i++)
  {
   var AllLines = this.state.output[n].lines[i];
            for (var t = 0 ; t < words.length ; t++)
            {
            var regex = new RegExp(words[t],"ig");
            var word = AllLines.match(regex);
            AllLines = AllLines.replace(word,`<span class="search-term">${word}</span>`);
            }
            

   lines.push(this.state.output[n].lines[i]);
   lines.push(<br/>)
  }
  return lines;
  } 

    toggleState(event) {
    event.preventDefault();
    //var name = event.target.href.split('/');
    this.props.app.setContentName(event.target.innerHTML);
   }

   createTable() {
   let table = []
   var count =0;
   //console.log(this.state.totalCount);
  
   if(this.state.totalCount > 5)
   {
     count = 5;
   }
   else  
   {
    count = this.state.totalCount;
   }
   for (let i = 0; i < count; i++) {
      //console.log(this.state.output[i].lines.length
      let lines = this.returnLines(i);
      table.push(<div className="result">
      <a className="result-name" onClick={this.toggleState} href={this.state.output[i].name} >{this.state.output[i].name}</a><br/>
      <p>{lines}</p>
      </div>);
    }
    return table
    //return this.state.totalCount
  } 

  render() {
    //@TODO
     
    return (
     <div>
     <form onSubmit={this.handleSubmit}>
        <span className="label">Search Terms:</span>
         <span className="control">
          <input type="text" value={this.state.value} onChange={this.handleChange} onBlur ={this.handleSubmit}   />
       </span>
       </form>
       <div>
       {this.createTable()}
       </div>
       <span className="error">{this.state.error}</span>
       </div>
   );
   
  }

}

module.exports = Search;
