//-*- mode: rjsx-mode;

'use strict';

const React = require('react');

class Content extends React.Component {

  /** called with properties:
   *  app: An instance of the overall app.  Note that props.app.ws
   *       will return an instance of the web services wrapper and
   *       props.app.setContentName(name) will set name of document
   *       in content tab to name and switch to content tab.
   *  name:Name of document to be displayed.
   */
  constructor(props) {
    super(props);
    //@TODO
    this.state = {content:''};
  }
  
  //@TODO
  async componentDidMount() {
     try {
       //console.log(this.props.name);
        var name  = this.props.name;
        if (typeof name != 'undefined'){
        //this.state = {content:''};
        const doccontent= await this.props.app.ws.getContent(name);
        //console.log(doccontent);
        this.setState({content: doccontent.content});}
     }
     catch (err) {
       const msg = (err.message) ? err.message : 'web service error';
       this.setState({errors: [msg]});
     }
   }


  
  async componentDidUpdate(prevProps) 
  { 
   try {
   if (this.props !== prevProps) 
   { 
    var name  = this.props.name;
     const doccontent= await this.props.app.ws.getContent(name);
        //console.log(doccontent);
        this.setState({content: doccontent.content});
   } 
   }
     catch (err) {
       const msg = (err.message) ? err.message : 'web service error';
       this.setState({errors: [msg]});
     }
   }
  render() {
    //@TODO
    return (
     <div>
     <section>
     <h1>{this.props.name}</h1>   
     <pre>{this.state.content}</pre>
     </section>
     </div>
    );
  }

}

module.exports = Content;
