//-*- mode: rjsx-mode;

'use strict';

const React = require('react');

class Add extends React.Component {

  /** called with properties:
   *  app: An instance of the overall app.  Note that props.app.ws
   *       will return an instance of the web services wrapper and
   *       props.app.setContentName(name) will set name of document
   *       in content tab to name and switch to content tab.
   */
  constructor(props) {
    super(props);
    //@TODO
     this.props = props;
     this.handleChange = this.handleChange.bind(this);
     this.state={ value:'',error:''};
  }

  //@TODO add code

  //Note that a you can get information on the file being uploaded by
  //hooking the change event on <input type="file">.  It will have
  //event.target.files[0] set to an object containing information
  //corresponding to the uploaded file.  You can get the contents
  //of the file by calling the provided readFile() function passing
  //this object as the argument.

    async handleChange(event) {
    try
    {
    this.setState({value: event.target.files[0]});
    var name  = event.target.files[0].name;
    //console.log(name);
    var filename = name.split(".");
    const filecontent = await readFile(event.target.files[0]);
    //console.log(filecontent);
    
    const output = await this.props.app.ws.addContent(filename[0],filecontent);
    this.props.app.setContentName(filename[0]);
    }
     catch (err) {
       const msg = (err.message) ? err.message : 'web service error';
       this.setState({error: [msg]});
     }
    }




  render() {
    //@TODO
    return (
    <div>
    <form>
    <label className="label">Choose File:<input className="control" type="file" onChange={this.handleChange}/></label>
    </form> 
    <span className="error">{this.state.error}</span>
    </div>
    );
  }

}

module.exports = Add;

/** Return contents of file (of type File) read from user's computer.
 *  The file argument is a file object corresponding to a <input
 *  type="file"/>
 */
async function readFile(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () =>  resolve(reader.result);
    reader.readAsText(file);
  });
}
